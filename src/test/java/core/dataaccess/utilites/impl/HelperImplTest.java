package core.dataaccess.utilites.impl;

import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class HelperImplTest {
    @Test
    public void getDateMonthDayYear() throws Exception {
        HelperImpl helper = new HelperImpl();
        String inputString = "2016-11-12";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date inputDate = dateFormat.parse(inputString);
        String[] actual = helper.getDateMonthDayYear(inputDate);
        assertEquals("2016", actual[0]);
        assertEquals("10", actual[1]);
        assertEquals("12", actual[2]);

    }

}