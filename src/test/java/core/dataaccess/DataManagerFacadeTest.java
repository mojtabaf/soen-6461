package core.dataaccess;

import core.model.StockInfo;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class DataManagerFacadeTest {
    @Test
    public void getStockInformation() throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String inputString = "2016-12-15";
        Date from = dateFormat.parse(inputString);
        inputString = "2016-12-19";
        Date to = dateFormat.parse(inputString);
        DataManagerFacade dataManagerFacade = DataManagerFacade.getInstance();
        List<StockInfo> api_result = dataManagerFacade.getStockInformation("WU", from, to, null);
        List<StockInfo> file_result = dataManagerFacade.getStockInformation("WU", from, to, "C:\\test\\data.csv");
        assertEquals(api_result.size(), 3);
        assertEquals(file_result.size(), 6);
    }
}