package core.model;

import java.util.Date;

/**
 * Created by mojtaba on 2/22/2017.
 */
public class MovingAvgInfo {
    private double movingAvg;
    private Date date;

    public MovingAvgInfo() {
    }

    public MovingAvgInfo(double movingAvg, Date date) {
        this.movingAvg = movingAvg;
        this.date = date;
    }

    public double getMovingAvg() {
        return movingAvg;
    }

    public void setMovingAvg(double movingAvg) {
        this.movingAvg = movingAvg;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
