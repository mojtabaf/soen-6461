package core.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class StockInfo implements Comparable {
    private String date;
    private String open;
    private String high;
    private String low;
    private String close;
    private String volume;
    private String adjClose;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getAdjClose() {
        return adjClose;
    }

    public void setAdjClose(String adjClose) {
        this.adjClose = adjClose;
    }

    @Override
    public int compareTo(Object o) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date thisDate = null;
        Date oDate = null;
        try {
            thisDate = dateFormat.parse(this.date);
            oDate = dateFormat.parse(((StockInfo)o).date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (thisDate.getTime()>= oDate.getTime())? 1: -1;
    }
}
