package core.model;
public class ErrorCell {
	private int row;
	private int col;
	public ErrorCell(int row) {
		super();
		this.row = row;
	}
	public ErrorCell(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}

}
