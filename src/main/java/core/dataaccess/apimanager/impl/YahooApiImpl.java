package core.dataaccess.apimanager.impl;
import core.dataaccess.apimanager.YahooApi;
import core.dataaccess.utilites.Helper;
import core.dataaccess.utilites.impl.HelperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class YahooApiImpl implements YahooApi {
    private Helper helper = new HelperImpl();
    public BufferedReader getInfoFromYahooApi(String symbol, Date from, Date to){
        String[] from_str = this.helper.getDateMonthDayYear(from);
        String[] to_str = this.helper.getDateMonthDayYear(to);
        String base_url = "http://ichart.finance.yahoo.com/table.csv?";
        base_url = base_url + "s=" + symbol + "&a=" + from_str[1] + "&b=" + from_str[2] +
                "&c=" + from_str[0] + "&d=" + to_str[1] + "&e=" + to_str[2] + "&f=" + to_str[0] +
                "&g=d&ignore=.csv";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> response = restTemplate.exchange(base_url, HttpMethod.GET, entity, String.class);
        InputStream is = new ByteArrayInputStream(response.getBody().getBytes());
        return new BufferedReader(new InputStreamReader(is));
    }
    @Override
    public ArrayList<ArrayList<String>> getApiResultIn2DArray(String symbol, Date from, Date to, String line_separator) throws IOException {
        BufferedReader br = this.getInfoFromYahooApi(symbol, from, to);
        ArrayList<ArrayList<String>> content = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            ArrayList<String> columns = new ArrayList<>();
            columns.addAll(this.helper.getCleanLine(line, line_separator));
            content.add(columns);
        }
        return content;
    }
}
