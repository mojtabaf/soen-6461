package core.dataaccess.apimanager;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mojtaba on 2/21/2017.
 */
public interface YahooApi {
    public ArrayList<ArrayList<String>> getApiResultIn2DArray
            (String symbol, Date from, Date to, String line_separator) throws IOException;
}
