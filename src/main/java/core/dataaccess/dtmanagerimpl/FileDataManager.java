package core.dataaccess.dtmanagerimpl;

import core.dataaccess.DataManager;
import core.dataaccess.filemanager.FileManager;
import core.dataaccess.filemanager.FileManagerFacade;
import core.model.ErrorCell;
import core.model.StockInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class FileDataManager extends DataManager {
    @Override
    public List<StockInfo> getStockInformation(String symbol, Date from, Date to, String line_separator) throws Exception {
        FileManagerFacade fileManagerFacade = FileManagerFacade.getInstance();
        boolean isValid = fileManagerFacade.isValid(this.getPath());
        if(!isValid){
            throw new Exception("selected file is not a csv file or the selected path is incorrect");
        }
        List<ErrorCell> corruptedCells = fileManagerFacade.getCorruptedCells(this.getPath());
        if(corruptedCells.size() > 0){
            String str = "";
            for (ErrorCell errorCell : corruptedCells) {
                if (errorCell.getCol() != 0) {
                    str += "Cell --> [" + errorCell.getRow() + " , " + errorCell.getCol() + "]\n";
                } else {
                    str += "Cell --> Row number " + errorCell.getRow() + " has more or less columns\n";
                }
            }
            throw new Exception(str);
        }
        ArrayList<ArrayList<String>> content = fileManagerFacade.getFileContentIn2DArray(this.getPath(), ",");
        return this.getStockInfoFrom2DArray(content, from, to, true);
    }
}
