package core.dataaccess.dtmanagerimpl;

import core.dataaccess.DataManager;
import core.dataaccess.apimanager.YahooApi;
import core.dataaccess.apimanager.impl.YahooApiImpl;
import core.model.StockInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class ApiDataManager extends DataManager {

    private YahooApi yahooApi = new YahooApiImpl();

    @Override
    public List<StockInfo> getStockInformation(String symbol, Date from, Date to, String line_separator) throws IOException, ParseException {
        ArrayList<ArrayList<String>> content = this.yahooApi.getApiResultIn2DArray(symbol, from, to, line_separator);
        return this.getStockInfoFrom2DArray(content, from, to, false);
    }
}
