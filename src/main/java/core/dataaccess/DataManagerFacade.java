package core.dataaccess;

import core.dataaccess.filemanager.FileManagerFacade;
import core.model.ErrorCell;
import core.model.StockInfo;
import org.springframework.stereotype.Controller;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class DataManagerFacade implements IDataManagerFacade {

    private static DataManagerFacade dataManagerFacade =  null;
    private FileManagerFacade fileManagerFacade = FileManagerFacade.getInstance();
    private DataManagerFacade() {
    }
    public static DataManagerFacade getInstance(){
        if(dataManagerFacade != null){
            return dataManagerFacade;
        }else{
            dataManagerFacade = new DataManagerFacade();
            return dataManagerFacade;
        }
    }
    public List<StockInfo> getStockInformation(String symbol, Date from, Date to, String filePath) throws Exception {
        DataManagerFactory dataManagerFactory = new DataManagerFactory();
        DataManager dataManager = null;
        if(filePath != null){
            dataManager = dataManagerFactory.getDataManager(Constant.FILE_SOURCE_TYPE);
            dataManager.setPath(filePath);
        }else{
            dataManager = dataManagerFactory.getDataManager(Constant.API_SOURCE_TYPE);
        }
        return dataManager.getStockInformation(symbol, from, to, ",");
    }
    public List<ErrorCell> getCorruptedCells(String fileName){
        return this.fileManagerFacade.getCorruptedCells(fileName);
	}
	
	public boolean isValid(String fileName){
        return this.fileManagerFacade.isValid(fileName);
	}
}
