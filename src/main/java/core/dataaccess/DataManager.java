package core.dataaccess;

import core.dataaccess.utilites.Helper;
import core.dataaccess.utilites.impl.HelperImpl;
import core.model.StockInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by mojtaba on 2/21/2017.
 */
public abstract class DataManager {

    private Helper helper = new HelperImpl();
    private String path;

    protected abstract List<StockInfo> getStockInformation(String symbol, Date from, Date to, String line_separator) throws Exception;
    public List<StockInfo> getStockInfoFrom2DArray(ArrayList<ArrayList<String>> content, Date from, Date to, boolean isFile) throws ParseException{
        ArrayList<String> header = content.get(0);
        List<StockInfo> catalog = new ArrayList<>();
        int line_number = 1;
        while (line_number < content.size()) {
            StockInfo stockInfo = new StockInfo();
            for (int i = 0; i < header.size(); i++) {
                this.helper.propertySetter(stockInfo, header.get(i), content.get(line_number).get(i));
            }
            catalog.add(stockInfo);
            line_number++;
        }
        if(isFile){
        	Collections.sort(catalog);
        	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        	catalog = catalog.stream().filter(c-> {
				try {
					return dateFormat.parse(c.getDate()).getTime() >= from.getTime() 
									&& dateFormat.parse(c.getDate()).getTime() <= to.getTime();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
			}).collect(Collectors.toList());
        }
        return catalog;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
