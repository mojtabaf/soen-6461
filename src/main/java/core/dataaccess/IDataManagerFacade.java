package core.dataaccess;

import core.model.ErrorCell;
import core.model.StockInfo;

import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/22/2017.
 */
public interface IDataManagerFacade {
    List<StockInfo> getStockInformation(String symbol, Date from, Date to, String filePath) throws Exception;
    List<ErrorCell> getCorruptedCells(String fileName);
	boolean isValid(String fileName);
}
