package core.dataaccess.utilites;

import core.model.StockInfo;

import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public interface Helper {
    List<String> getCleanLine(String line, String line_separator);
    String[] getDateMonthDayYear(Date date);
    void propertySetter(StockInfo stockInfo, String header, String value);
}
