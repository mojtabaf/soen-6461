package core.dataaccess.utilites.impl;

import core.dataaccess.utilites.Helper;
import core.model.StockInfo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
@Component
public class HelperImpl implements Helper {
    public List<String> getCleanLine(String line, String line_separator){
        String[] line_content = line.split(line_separator);
        for (int i = 0; i < line_content.length; i++){
            line_content[i] = line_content[i].replaceAll("\"", "");
            line_content[i] = line_content[i].replaceAll("\\s+", "").trim();
        }
        return Arrays.asList(line_content);
    }

    @Override
    public String[] getDateMonthDayYear(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String str = formatter.format(date);
        String[] result = str.split("-");
        result[1] = String.valueOf((Integer.parseInt(result[1]) - 1));
        return result;
    }

    public void propertySetter(StockInfo stockInfo, String header, String value){
        switch (header) {
            case "Date":
                stockInfo.setDate(value);
                break;
            case "Open":
                stockInfo.setOpen(value);
                break;
            case "High":
                stockInfo.setHigh(value);
                break;
            case "Low":
                stockInfo.setLow(value);
                break;
            case "Close":
                stockInfo.setClose(value);
                break;
            case "Volume":
                stockInfo.setVolume(value);
                break;
            case "AdjClose":
                stockInfo.setAdjClose(value);
                break;
            case "Adj Close":
                stockInfo.setAdjClose(value);
                break;
            default:
                break;
        }
    }
}
