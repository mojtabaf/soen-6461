package core.dataaccess;

import core.dataaccess.dtmanagerimpl.ApiDataManager;
import core.dataaccess.dtmanagerimpl.FileDataManager;
import org.springframework.stereotype.Service;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class DataManagerFactory {
    private String path;
    public DataManager getDataManager(String sourceType){
        if(sourceType == null){
            return null;
        }
        if(sourceType.equalsIgnoreCase("API")){
            return new ApiDataManager();

        } else if(sourceType.equalsIgnoreCase("FILE")){
            return new FileDataManager();
        }
        return null;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
