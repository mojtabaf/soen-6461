package core.dataaccess.filemanager;

import core.dataaccess.filemanager.impl.*;
import core.model.ErrorCell;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public interface FileValidator {
    public List<ErrorCell> getCorruptedCells(String fileName);
    public boolean isValid(String fileName);
}
