package core.dataaccess.filemanager;

import core.dataaccess.filemanager.impl.*;
import core.model.ErrorCell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public interface FileManager {
    public ArrayList<ArrayList<String>> readTextFileTo2dArrayList(
            String path, String line_separator);
    public List<ErrorCell> getCorruptedCells(String path);
    public boolean isValid(String path);
}
