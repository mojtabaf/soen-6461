package core.dataaccess.filemanager;

import java.util.ArrayList;

/**
 * Created by mojtaba on 2/21/2017.
 */
public interface FileImport {
    public ArrayList<ArrayList<String>> readTextFileTo2dArrayList(
            String path, String line_separator);

}
