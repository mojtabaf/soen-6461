package core.dataaccess.filemanager.impl;

import core.dataaccess.filemanager.FileImport;
import core.dataaccess.filemanager.FileManager;
import core.dataaccess.filemanager.FileValidator;
import core.model.ErrorCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mojtaba on 2/21/2017.
 */
public class FileManagerImpl implements FileManager {

    private FileImport fileImport = new FileImportImpl();

    private FileValidator fileValidator = new FileValidatorImpl();

    public ArrayList<ArrayList<String>> readTextFileTo2dArrayList(
            String path, String line_separator){
        return fileImport.readTextFileTo2dArrayList(path, line_separator);
    }
    public List<ErrorCell> getCorruptedCells(String path){
        return fileValidator.getCorruptedCells(path);
    }

    public boolean isValid(String path){
        return fileValidator.isValid(path);
    }
}
