package core.dataaccess.filemanager.impl;

import core.dataaccess.filemanager.FileImport;
import core.dataaccess.utilites.Helper;
import core.dataaccess.utilites.impl.HelperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 * Created by mojtaba on 2/21/2017.
 */
public class FileImportImpl implements FileImport {

	private Helper helper = new HelperImpl();

	public ArrayList<ArrayList<String>> readTextFileTo2dArrayList(
			String path, String line_separator) {
		ArrayList<ArrayList<String>> content = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line;
			while ((line = br.readLine()) != null) {
				ArrayList<String> cols = new ArrayList<>();
				cols.addAll(this.helper.getCleanLine(line, line_separator));
				content.add(cols);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
}
