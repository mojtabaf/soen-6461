package core.dataaccess.filemanager.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import core.dataaccess.filemanager.FileImport;
import core.dataaccess.filemanager.FileValidator;
import core.model.ErrorCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Created by mojtaba on 2/21/2017.
 */
public class FileValidatorImpl implements FileValidator {

	private FileImport fileImport = new FileImportImpl();
	private boolean isCSVFile(String fileName) {
		String[] strs = fileName.split("\\.");
		if (strs.length <= 1) {
			return false;
		} else {
			if (strs[strs.length - 1].equalsIgnoreCase("csv")) {
				return true;
			} else {
				return false;
			}
		}
	}
	private boolean isMissing(String fileName) {
		File f = new File(fileName);
		if (f.exists()) {
			return false;
		} else {
			return true;
		}
	}
	public List<ErrorCell> getCorruptedCells(String fileName){
		List<ErrorCell> errors = new ArrayList<>();
		ArrayList<ArrayList<String>> dataGrid = fileImport.readTextFileTo2dArrayList(fileName, ",");
		int row = 0;
		int col = 0;
		int col_num = dataGrid.get(0).size();
		for (ArrayList<String> arrayList : dataGrid) {
			if(col_num != arrayList.size()){
				ErrorCell cell =  new ErrorCell(row);
				errors.add(cell);
			}else{
				for(String str : arrayList){
					if(str.equalsIgnoreCase("")){
						ErrorCell cell =  new ErrorCell(row,col);
						errors.add(cell);
					}
					col++;
				}
			}
			row++;
			col = 0;
		}
		return errors;
	}
	public boolean isValid(String fileName){
		
		if(!isCSVFile(fileName)){
			return false;
		}
		if(isMissing(fileName)){
			return false;
		}
		return true;
	}
}
