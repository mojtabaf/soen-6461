package core.dataaccess.filemanager;

import java.util.ArrayList;
import java.util.List;

import core.dataaccess.filemanager.impl.FileManagerImpl;
import core.model.ErrorCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Created by mojtaba on 2/21/2017.
 */
public class FileManagerFacade {
    private FileManager fileManager = new FileManagerImpl();

	private static FileManagerFacade fileManagerFacade =  null;
	private FileManagerFacade() {
	}
	public static FileManagerFacade getInstance(){
		if(fileManagerFacade != null){
			return fileManagerFacade;
		}else{
			fileManagerFacade = new FileManagerFacade();
			return fileManagerFacade;
		}
	}
	public ArrayList<ArrayList<String>> getFileContentIn2DArray(String path, String line_separator){
        return fileManager.readTextFileTo2dArrayList(path, line_separator);
	}
	public List<ErrorCell> getCorruptedCells(String fileName){
        return fileManager.getCorruptedCells(fileName);
	}
	
	public boolean isValid(String fileName){
        return fileManager.isValid(fileName);
	}

}
