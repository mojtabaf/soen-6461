package core.applicationservice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import core.model.MovingAvgInfo;
import core.model.StockInfo;
import ui.Utility;

public class WatchListService implements IWatchListService{
	private IStockInfoService stockInfoService = new StockInfoService(); 

	@Override
	public Map<String, Boolean> getWatchListDetails(List<String> companies_name, int periodOfTime, int firstMovingAvgPeriod, int secondMovingAvgPeriod) throws Exception {
		Map<String, Boolean> map =  new HashMap<>();
		List<String> symbols = Utility.getSymbolsFromCompaniesName(companies_name);
		int index = 0;
		for(String symbol: symbols){
			String company_name = companies_name.get(index);
			map.put(company_name + "\t" + symbol, isValuableToBy(symbol, periodOfTime, firstMovingAvgPeriod, secondMovingAvgPeriod));
			index++;
		}
		return map;
	}
	
	private boolean isValuableToBy(String company_symbol, int periodOfTime, int firstMovingAvgPeriod, int secondMovingAvgPeriod) throws Exception{
		List<StockInfo> stockInfoData = stockInfoService.getApiStockInfo(company_symbol, periodOfTime);
		List<MovingAvgInfo> first_avgInfos = stockInfoService.getMovingAverage(stockInfoData, firstMovingAvgPeriod); 
		List<MovingAvgInfo> second_avgInfos = stockInfoService.getMovingAverage(stockInfoData, secondMovingAvgPeriod);
		MovingAvgInfo match = null;
		for(int i = second_avgInfos.size() -1; i >= 0 ; i--){
			MovingAvgInfo mvg = second_avgInfos.get(i);
			match = first_avgInfos.stream().filter(m -> 
				mvg.getMovingAvg() == m.getMovingAvg() && mvg.getDate().getTime() == m.getDate().getTime()
			).findFirst().orElse(null);
			if(match != null){
				break;
			}
		}
		if(match == null){
			MovingAvgInfo mvg = second_avgInfos.get(second_avgInfos.size() -1);
			if(mvg.getMovingAvg() < Double.parseDouble(stockInfoData.get(stockInfoData.size() - 1).getClose())){
				return true;
			}else{
				return false;
			}
		}else{
			if(match.getMovingAvg() < Double.parseDouble(stockInfoData.get(stockInfoData.size() - 1).getClose())){
				return true;
			}else{
				return false;
			}
		}
	}

	@Override
	public void saveWatchListToFile(String path) {
		WatchList watchList = WatchList.getInstance();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
			for(String company: watchList.getWatchSources()){
				bw.write(company);
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void importWatchListFromFile(String path) {
		WatchList watchList = WatchList.getInstance();
		List<String> watchListSetting = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line;
			while ((line = br.readLine()) != null) {
				watchListSetting.add(line.trim());
			}
			watchList.setWatchSources(watchListSetting);

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
