package core.applicationservice;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IWatchListService {
	Map<String, Boolean> getWatchListDetails(List<String> companies_name, int periodOfTime, 
			int firstMovingAvgPeriod, int secondMovingAvgPeriod) throws Exception;
	void saveWatchListToFile(String path);
	void importWatchListFromFile(String path);
}
