package core.applicationservice;

import core.dataaccess.DataManagerFacade;
import core.dataaccess.IDataManagerFacade;
import core.model.ErrorCell;
import core.model.MovingAvgInfo;
import core.model.StockInfo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mojtaba on 2/22/2017.
 */
public class StockInfoService implements IStockInfoService {
    private IDataManagerFacade dataManagerFacade = DataManagerFacade.getInstance();
    @Override
    public List<StockInfo> getFileStockInfo(String symbol, String filePath, Date from, Date to) throws Exception {
        return this.dataManagerFacade.getStockInformation(symbol, from, to, filePath);
    }

    @Override
    public List<StockInfo> getFileStockInfo(String symbol, String filePath, int periodOFTime) throws Exception {
        Date from = getNDaysBeforeDate(periodOFTime);
        Date today = new Date();
        return this.dataManagerFacade.getStockInformation(symbol, from, today, filePath);
    }

    @Override
    public List<StockInfo> getApiStockInfo(String symbol, Date from, Date to) throws Exception {
        return this.dataManagerFacade.getStockInformation(symbol, from, to, null);
    }

    @Override
    public List<StockInfo> getApiStockInfo(String symbol, int periodOFTime) throws Exception {
        Date from = getNDaysBeforeDate(periodOFTime);
        Date today = new Date();
        return this.dataManagerFacade.getStockInformation(symbol, from, today, null);
    }

    @Override
    public List<MovingAvgInfo> getMovingAverage(List<StockInfo> stockInfoData, int movingAvgDays) throws Exception {
        if(movingAvgDays <= 1){
            throw new Exception("moving average days should be more that one");
        }
        //List<StockInfo> stockData = this.dataManagerFacade.getStockInformation(symbol, from, to, filePath);
        Collections.sort(stockInfoData);
        return getMovingAvgInfos(movingAvgDays, stockInfoData);
    }

//    @Override
//    public List<MovingAvgInfo> getFileMovingAverage(String symbol, String filePath, int periodOFTime, int movingAvgDays) throws Exception {
//        if(movingAvgDays <= 1){
//            throw new Exception("moving average days should be more that one");
//        }
//        List<StockInfo> stockInfoData = this.getFileStockInfo(symbol, filePath, periodOFTime);
//        Collections.sort(stockInfoData);
//        return getMovingAvgInfos(movingAvgDays, stockInfoData);
//    }

//    @Override
//    public List<MovingAvgInfo> getApiMovingAverage(String symbol, Date from, Date to, int movingAvgDays) throws Exception {
//        if(movingAvgDays <= 1){
//            throw new Exception("moving average days should be more that one");
//        }
//        List<StockInfo> stockInfoData = this.getApiStockInfo(symbol, from, to);
//        Collections.sort(stockInfoData);
//        return getMovingAvgInfos(movingAvgDays, stockInfoData);
//    }
//
//    @Override
//    public List<MovingAvgInfo> getApiMovingAverage(String symbol, int periodOFTime, int movingAvgDays) throws Exception {
//        if(movingAvgDays <= 1){
//            throw new Exception("moving average days should be more that one");
//        }
//        List<StockInfo> stockInfoData = this.getApiStockInfo(symbol, periodOFTime);
//        Collections.sort(stockInfoData);
//        return getMovingAvgInfos(movingAvgDays, stockInfoData);
//    }


    private Date getNDaysBeforeDate(int periodOFTime){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_YEAR, -1 * periodOFTime);
        return cal.getTime();
    }

    private List<MovingAvgInfo> getMovingAvgInfos(int movingAvgDays, List<StockInfo> stockData) throws ParseException {
        List<MovingAvgInfo> movingAvgData = new ArrayList<>();
        for(int i = 0; i < stockData.size(); i++){
            int j = i;
            double sum = 0;
            if((i + movingAvgDays -1)>= stockData.size()){
                break;
            }else{
                for(int k = 0; k < movingAvgDays;k++){
                    sum += new Double(stockData.get(j).getClose());
                    j++;
                }
                double mv = sum /movingAvgDays;
                int middle = movingAvgDays / 2;
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date mv_date = dateFormat.parse(stockData.get(i + middle).getDate());
                MovingAvgInfo movingAvgInfo = new MovingAvgInfo(mv, mv_date);
                movingAvgData.add(movingAvgInfo);
            }
        }
        return movingAvgData;
    }

	@Override
	public List<ErrorCell> getCorruptedCells(String fileName) {
		return this.dataManagerFacade.getCorruptedCells(fileName);
	}

	@Override
	public boolean isValid(String fileName) {
		return this.dataManagerFacade.isValid(fileName);
	}
}
