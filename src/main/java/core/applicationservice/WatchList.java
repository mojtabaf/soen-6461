package core.applicationservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WatchList {
	private static WatchList instance = null;
	private List<String> sources = new ArrayList<>();
	private List<String> watchSources = new ArrayList<>();

	private WatchList() {

	}

	public static WatchList getInstance() {
		if (instance == null) {
			instance = new WatchList();

			List<String> lst = createSourceList();
			instance.setSources(lst);
			List<String> watchList = createWatchList();
			instance.setWatchSources(watchList);
		}
		return instance;
	}

	private static List<String> createSourceList() {
		List<String> lst = new ArrayList<>();
		lst.add("Wal-Mart Stores");
		lst.add("Johnson & Johnson");
		lst.add("3M");
		lst.add("United Technologies");
		lst.add("Procter & Gamble");
		lst.add("Pfizer");
		lst.add("Verizon");
		lst.add("Microsoft");
		lst.add("Coca-Cola Co.");
		lst.add("Merck");
		lst.add("Intel Corp");
		lst.add("Travelers Co.");
		lst.add("Home Depot");
		lst.add("General Electric");
		lst.add("Boeing Co.");
		lst.add("American Express");
		lst.add("Goldman Sachs");
		lst.add("Nike Inc.");
		lst.add("The Walt Disney Company");
		lst.add("Apple Inc.");
		lst.add("UnitedHealth Group");
		lst.add("Visa");
		lst.add("DuPont");
		lst.add("Exxon Mobil");
		lst.add("JP Morgan Chase");
		lst.add("Chevron Corp");
		lst.add("Caterpillar Inc.");
		lst.add("McDonald's");
		return lst;
	}

	private static List<String> createWatchList() {
		List<String> lst = new ArrayList<>();
		lst.add("IBM Corp");
		lst.add("Cisco Systems");
		return lst;
	}

	public void removeFromSource(String str) {
		this.getSources().remove(str);
		this.getWatchSources().add(str);
	}

	public void removeFromWatchList(String str) {
		this.sources.add(str);
		this.watchSources.remove(str);
	}

	public List<String> getSources() {
		return sources;
	}

	public void setSources(List<String> sources) {
		this.sources = sources;
	}

	public List<String> getWatchSources() {
		return watchSources;
	}

	public void setWatchSources(List<String> watchSources) {
		this.watchSources = watchSources;
	}

}
