package core.applicationservice;

import core.model.ErrorCell;
import core.model.MovingAvgInfo;
import core.model.StockInfo;

import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 2/22/2017.
 */
public interface IStockInfoService {
    List<StockInfo> getFileStockInfo(String symbol, String filePath, Date from, Date to) throws Exception;

    List<StockInfo> getFileStockInfo(String symbol, String filePath, int periodOFTime) throws Exception;

    List<StockInfo> getApiStockInfo(String symbol, Date from, Date to) throws Exception;

    List<StockInfo> getApiStockInfo(String symbol, int periodOFTime) throws Exception;
    
    List<MovingAvgInfo> getMovingAverage(List<StockInfo> stockInfoData, int movingAvgDays) throws Exception;

//    List<MovingAvgInfo> getFileMovingAverage(String symbol, String filePath, Date from, Date to, int movingAvgDays) throws Exception;
//
//    List<MovingAvgInfo> getFileMovingAverage(String symbol, String filePath, int periodOFTime, int movingAvgDays) throws Exception;
//
//    List<MovingAvgInfo> getApiMovingAverage(String symbol, Date from, Date to, int movingAvgDays) throws Exception;
//
//    List<MovingAvgInfo> getApiMovingAverage(String symbol, int periodOFTime, int movingAvgDays) throws Exception;
    List<ErrorCell> getCorruptedCells(String fileName);
	boolean isValid(String fileName);
}
