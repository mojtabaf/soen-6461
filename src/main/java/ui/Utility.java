package ui;

import java.util.ArrayList;
import java.util.List;

public class Utility {

	public static List<String> getSymbolsFromCompaniesName(List<String> companies_name){
		List<String> symbols = new ArrayList<>();
		for(String company: companies_name){
			symbols.add(getSymbolFromName(company));
		}
		return symbols;
	}
	
	public static String getSymbolFromName(String companyName){
		String symbol = "";
		switch (companyName) {
		case "Wal-Mart Stores":
			symbol = "WMT";
			break;
		case "Johnson & Johnson":
			symbol = "JNJ";
			break;
		case "3M":
			symbol = "MMM";
			break;
		case "United Technologies":
			symbol = "UTX";
			break;
		case "Procter & Gamble":
			symbol = "PG";
			break;
		case "Pfizer":
			symbol = "PFE";
			break;
		case "Verizon":
			symbol = "VZ";
			break;
		case "Microsoft":
			symbol = "MSFT";
			break;
		case "Coca-Cola Co.":
			symbol = "KO";
			break;
		case "Merck":
			symbol = "MRK";
			break;
		case "Intel Corp":
			symbol = "INTC";
			break;
		case "Travelers Co.":
			symbol = "TRV";
			break;
		case "Home Depot":
			symbol = "HD";
			break;
		case "General Electric":
			symbol = "GE";
			break;
		case "Boeing Co.":
			symbol = "BA";
			break;
		case "American Express":
			symbol = "AXP";
			break;
		case "Goldman Sachs":
			symbol = "GS";
			break;
		case "Nike Inc.":
			symbol = "NKE";
			break;
		case "The Walt Disney Company":
			symbol = "DIS";
			break;
		case "Apple Inc.":
			symbol = "AAPL";
			break;
		case "UnitedHealth Group":
			symbol = "UHN";
			break;
		case "Visa":
			symbol = "V";
			break;
		case "Cisco Systems":
			symbol = "CSCO";
			break;
		case "IBM Corp":
			symbol = "IBM";
			break;
		case "DuPont":
			symbol = "DD";
			break;
		case "Exxon Mobil":
			symbol = "XOM";
			break;
		case "JP Morgan Chase":
			symbol = "JPM";
			break;
		case "Chevron Corp":
			symbol = "CVX";
			break;
		case "Caterpillar Inc.":
			symbol = "CAT";
			break;
		case "McDonald's":
			symbol = "MCD";
			break;
		}
		return symbol;
	}

}
