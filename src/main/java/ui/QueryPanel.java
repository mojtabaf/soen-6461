package ui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.jfree.chart.ChartPanel;

import core.applicationservice.IStockInfoService;
import core.applicationservice.StockInfoService;
import core.model.StockInfo;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import java.awt.Choice;

public class QueryPanel extends JPanel {
	private JComboBox queryTypeComboBox = new JComboBox();
	private JDatePickerImpl from_datePicker;
	private JDatePickerImpl to_datePicker;
	private JTextField textField_last_days = new JTextField();
	private JLabel lblFrom = new JLabel("From:");
	private JLabel lblTo = new JLabel("To:");
	private ChartPanel chartPanel = null;
	private IStockInfoService stockInfoService = new StockInfoService();
	private final JTextArea textArea = new JTextArea();
	private final JLabel lblFirstAveragingPeriod = new JLabel("First Averaging Period:");
	private final JLabel lblSecondAveragingPeriod = new JLabel("Second Averaging Period:");
	private final JTextField textField_first_mAvg = new JTextField();
	private final JTextField textField_second_mAvg = new JTextField();
	private boolean readFromFile;
	private String filePath;
	private final JButton btnBuy = new JButton("Select a Cross Point");
	private final JLabel lbl_recomendation_date = new JLabel("");
	private final JComboBox comboBox = new JComboBox();

	public JLabel getLbl_recomendation_date() {
		return lbl_recomendation_date;
	}
	
	public JTextArea getTextArea() {
		return textArea;
	}

	public ChartPanel getChartPanel() {
		return chartPanel;
	}

	public JButton getBtnBuy() {
		return btnBuy;
	}

	/**
	 * Create the panel.
	 */
	public QueryPanel(boolean readfromFile) {
		this.setReadFromFile(readfromFile);
		textField_first_mAvg.setColumns(10);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 46, 164, 0, 65, 46, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 30, 400, 38, 128, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblCompanyLable = new JLabel("Company:");
		GridBagConstraints gbc_lblCompanyLable = new GridBagConstraints();
		gbc_lblCompanyLable.anchor = GridBagConstraints.WEST;
		gbc_lblCompanyLable.insets = new Insets(0, 0, 5, 5);
		gbc_lblCompanyLable.gridx = 1;
		gbc_lblCompanyLable.gridy = 0;
		add(lblCompanyLable, gbc_lblCompanyLable);

		JButton btnSearch = new JButton("Get Information");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					onSearchClick();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 2;
		gbc_comboBox.gridy = 0;
		comboBox.setModel(new DefaultComboBoxModel(getDow30Model()));
		add(comboBox, gbc_comboBox);
		GridBagConstraints gbc_btnSearch = new GridBagConstraints();
		gbc_btnSearch.insets = new Insets(0, 0, 5, 5);
		gbc_btnSearch.gridx = 3;
		gbc_btnSearch.gridy = 0;
		add(btnSearch, gbc_btnSearch);

		JLabel lblSelectQueryType = new JLabel("Select Query Type:");
		GridBagConstraints gbc_lblSelectQueryType = new GridBagConstraints();
		gbc_lblSelectQueryType.anchor = GridBagConstraints.WEST;
		gbc_lblSelectQueryType.insets = new Insets(0, 0, 5, 5);
		gbc_lblSelectQueryType.gridx = 1;
		gbc_lblSelectQueryType.gridy = 1;
		add(lblSelectQueryType, gbc_lblSelectQueryType);
		this.queryTypeComboBox
				.setModel(new DefaultComboBoxModel(new String[] { "By Date Range", "By Number of Last Days" }));
		GridBagConstraints gbc_queryTypeComboBox = new GridBagConstraints();
		gbc_queryTypeComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_queryTypeComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_queryTypeComboBox.gridx = 2;
		gbc_queryTypeComboBox.gridy = 1;
		add(queryTypeComboBox, gbc_queryTypeComboBox);

		GridBagConstraints gbc_textField_last_days = new GridBagConstraints();
		gbc_textField_last_days.insets = new Insets(0, 0, 5, 5);
		gbc_textField_last_days.gridx = 3;
		gbc_textField_last_days.gridy = 1;
		this.textField_last_days.setText("");
		add(textField_last_days, gbc_textField_last_days);
		textField_last_days.setColumns(10);

		GridBagConstraints gbc_lblAveragingPeriod = new GridBagConstraints();
		gbc_lblAveragingPeriod.anchor = GridBagConstraints.WEST;
		gbc_lblAveragingPeriod.insets = new Insets(0, 0, 5, 5);
		gbc_lblAveragingPeriod.gridx = 1;
		gbc_lblAveragingPeriod.gridy = 2;
		add(lblFirstAveragingPeriod, gbc_lblAveragingPeriod);

		GridBagConstraints gbc_textField_mAvg = new GridBagConstraints();
		gbc_textField_mAvg.insets = new Insets(0, 0, 5, 5);
		gbc_textField_mAvg.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_mAvg.gridx = 2;
		gbc_textField_mAvg.gridy = 2;
		this.textField_first_mAvg.setText("");
		add(textField_first_mAvg, gbc_textField_mAvg);
		

		GridBagConstraints gbc_lblSecondAveragingPeriod = new GridBagConstraints();
		gbc_lblSecondAveragingPeriod.anchor = GridBagConstraints.WEST;
		gbc_lblSecondAveragingPeriod.insets = new Insets(0, 0, 5, 5);
		gbc_lblSecondAveragingPeriod.gridx = 1;
		gbc_lblSecondAveragingPeriod.gridy = 3;
		add(lblSecondAveragingPeriod, gbc_lblSecondAveragingPeriod);

		GridBagConstraints gbc_secondTextField_mAvg = new GridBagConstraints();
		gbc_secondTextField_mAvg.insets = new Insets(0, 0, 5, 5);
		gbc_secondTextField_mAvg.fill = GridBagConstraints.HORIZONTAL;
		gbc_secondTextField_mAvg.gridx = 2;
		gbc_secondTextField_mAvg.gridy = 3;
		this.textField_second_mAvg.setText("");
		add(textField_second_mAvg, gbc_secondTextField_mAvg);

		GridBagConstraints gbc_lblFrom = new GridBagConstraints();
		gbc_lblFrom.anchor = GridBagConstraints.WEST;
		gbc_lblFrom.insets = new Insets(0, 0, 5, 5);
		gbc_lblFrom.gridx = 1;
		gbc_lblFrom.gridy = 4;
		add(lblFrom, gbc_lblFrom);

		UtilDateModel from_model = new UtilDateModel();
		JDatePanelImpl from_datePanel = new JDatePanelImpl(from_model);
		this.from_datePicker = new JDatePickerImpl(from_datePanel);
		GridBagConstraints gbc_from_datePicker = new GridBagConstraints();
		gbc_from_datePicker.fill = GridBagConstraints.HORIZONTAL;
		gbc_from_datePicker.insets = new Insets(0, 0, 5, 5);
		gbc_from_datePicker.gridx = 2;
		gbc_from_datePicker.gridy = 4;
		add(from_datePicker, gbc_from_datePicker);

		GridBagConstraints gbc_lblTo = new GridBagConstraints();
		gbc_lblTo.anchor = GridBagConstraints.WEST;
		gbc_lblTo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTo.gridx = 1;
		gbc_lblTo.gridy = 5;
		add(lblTo, gbc_lblTo);

		UtilDateModel to_model = new UtilDateModel();
		JDatePanelImpl to_datePanel = new JDatePanelImpl(to_model);
		this.to_datePicker = new JDatePickerImpl(to_datePanel);
		GridBagConstraints gbc_to_datePicker = new GridBagConstraints();
		gbc_to_datePicker.fill = GridBagConstraints.HORIZONTAL;
		gbc_to_datePicker.insets = new Insets(0, 0, 5, 5);
		gbc_to_datePicker.gridx = 2;
		gbc_to_datePicker.gridy = 5;
		add(to_datePicker, gbc_to_datePicker);

		Border border = BorderFactory.createLineBorder(Color.gray, 2);
		textArea.setBorder(border);
		textArea.setVisible(false);
		textArea.setEditable(false);
		textArea.setColumns(20);
		textArea.setRows(5);
		textArea.setTabSize(16);
		
				GridBagConstraints gbc_textArea = new GridBagConstraints();
				gbc_textArea.insets = new Insets(0, 0, 0, 5);
				gbc_textArea.gridwidth = 3;
				gbc_textArea.fill = GridBagConstraints.BOTH;
				gbc_textArea.gridx = 1;
				gbc_textArea.gridy = 8;
				add(textArea, gbc_textArea);

		showDateRange(true);
		this.queryTypeComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onQueryTypeComboBoxChanged();
			}
		});

	}

	private void onQueryTypeComboBoxChanged() {
		// this.chartPanel.setVisible(false);
		if (queryTypeComboBox.getSelectedIndex() == 0) {
			showDateRange(true);
		} else {
			showDateRange(false);
		}
	}

	private void showDateRange(boolean show) {
		this.lblFrom.setVisible(show);
		this.lblTo.setVisible(show);
		this.to_datePicker.setVisible(show);
		this.from_datePicker.setVisible(show);
		this.from_datePicker.getModel().setValue(null);
		this.to_datePicker.getModel().setValue(null);
		this.textField_last_days.setVisible(!show);
	}

	private void onSearchClick() throws Exception {
		if (this.parameterExist()) {
			// this.chartPanel.setVisible(true);
			// remove(chartPanel);
			this.textArea.setVisible(true);
			extractData();
		} else {
			// this.chartPanel.setVisible(false);
			// remove(chartPanel);
			JOptionPane.showMessageDialog(null, "please enter query parameter!\n  And please consider that first moving average should be les than second one", "Warning",
					JOptionPane.WARNING_MESSAGE);
		}

	}

	private boolean parameterExist() {
//		if (this.symbol_textField.getText().trim().equalsIgnoreCase("")) {
//			return false;
//		}
		if (this.textField_first_mAvg.getText().trim().equalsIgnoreCase("")) {
			return false;
		}
		if (this.textField_second_mAvg.getText().trim().equalsIgnoreCase("")) {
			return false;
		}
		int first = Integer.parseInt(this.textField_first_mAvg.getText().trim());
		int second = Integer.parseInt(this.textField_second_mAvg.getText().trim());
		if(first >= second){
			return false;
		}
		if (this.queryTypeComboBox.getSelectedIndex() == 0 && (this.from_datePicker.getModel().getValue() == null
				|| this.to_datePicker.getModel().getValue() == null)) {
			return false;
		}
		if (this.queryTypeComboBox.getSelectedIndex() == 1 && this.textField_last_days.getText().equalsIgnoreCase("")) {
			return false;
		}
		return true;
	}

	private void extractData() throws Exception {
		List<StockInfo> stockInfo = new ArrayList<>();
		String selected_company_name = (String) this.comboBox.getSelectedItem();
		String symbol = Utility.getSymbolFromName(selected_company_name);
		if (this.queryTypeComboBox.getSelectedIndex() == 0) {
			Date from = (Date) this.from_datePicker.getModel().getValue();
			Date to = (Date) this.to_datePicker.getModel().getValue();
			
			if (this.isReadFromFile()) {
				stockInfo = this.stockInfoService.getFileStockInfo(symbol.trim(),
						this.getFilePath(), from, to);
			} else {
				stockInfo = this.stockInfoService.getApiStockInfo(symbol.trim(), from, to);
			}

		} else {
			int period = Integer.parseInt(this.textField_last_days.getText().trim());
			if (this.isReadFromFile()) {
				stockInfo = this.stockInfoService.getFileStockInfo(symbol.trim(),
						this.getFilePath(), period);
			} else {
				stockInfo = this.stockInfoService.getApiStockInfo(symbol.trim(), period);
			}
		}
		createChart(stockInfo);
		textArea.setVisible(true);
	}

	private void createChart(List<StockInfo> stockInfo) {
		ChartBuilder chartBuilder = new ChartBuilder();
		if (chartPanel != null && btnBuy != null) {
			this.remove(chartPanel);
			this.remove(btnBuy);
			this.remove(lbl_recomendation_date);
			this.revalidate();
			this.repaint();
		}
		String selected_company_name = (String) this.comboBox.getSelectedItem();
		String symbol = Utility.getSymbolFromName(selected_company_name);
		String header = selected_company_name + " (" + symbol.trim() + ")" + " Stock Information";
		int first = Integer.parseInt(this.textField_first_mAvg.getText().trim());
		int second = Integer.parseInt(this.textField_second_mAvg.getText().trim());
		this.chartPanel = chartBuilder.createChartPanel(header,
				stockInfo, this.textArea, first, second, this.btnBuy, this.lbl_recomendation_date);

		GridBagConstraints gbc_chartPanel = new GridBagConstraints();
		gbc_chartPanel.gridwidth = 3;
		gbc_chartPanel.insets = new Insets(0, 0, 0, 5);
		gbc_chartPanel.fill = GridBagConstraints.BOTH;
		gbc_chartPanel.gridx = 1;
		gbc_chartPanel.gridy = 6;
		add(chartPanel, gbc_chartPanel);

		GridBagConstraints gbc_btnBuy = new GridBagConstraints();
		gbc_btnBuy.insets = new Insets(0, 0, 5, 5);
		gbc_btnBuy.gridx = 3;
		gbc_btnBuy.gridy = 7;
		btnBuy.setBackground(Color.WHITE);
		add(btnBuy, gbc_btnBuy);
		
		GridBagConstraints gbc_lbl_recomendation_date = new GridBagConstraints();
		gbc_lbl_recomendation_date.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_recomendation_date.gridx = 2;
		gbc_lbl_recomendation_date.gridy = 7;
		lbl_recomendation_date.setText("");
		add(lbl_recomendation_date, gbc_lbl_recomendation_date);
		
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.gridwidth = 3;
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 1;
		gbc_textArea.gridy = 8;
		this.textArea.setVisible(true);
		add(textArea, gbc_textArea);
		this.revalidate();
		this.repaint();

	}

	public boolean isReadFromFile() {
		return readFromFile;
	}

	public void setReadFromFile(boolean readFromFile) {
		this.readFromFile = readFromFile;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	private String[] getDow30Model(){
		return new String[]{
				"Wal-Mart Stores",
				"Johnson & Johnson",
				"3M",
				"United Technologies",
				"Procter & Gamble",
				"Pfizer",
				"Verizon",
				"Microsoft",
				"Coca-Cola Co.",
				"Merck",
				"Intel Corp",
				"Travelers Co.",
				"Home Depot",
				"General Electric",
				"Boeing Co.",
				"American Express",
				"Goldman Sachs",
				"Nike Inc.",
				"The Walt Disney Company",
				"Apple Inc.",
				"UnitedHealth Group",
				"Visa",
				"Cisco Systems",
				"IBM Corp",
				"DuPont",
				"Exxon Mobil",
				"JP Morgan Chase",
				"Chevron Corp",
				"Caterpillar Inc.",
				"McDonald's"
		};
	}

}
