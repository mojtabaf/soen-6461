/*
 * The Chart Builder class
 * Builds a chart using jfree library
 */
package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.MinMaxCategoryRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.MovingAverage;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

import core.model.StockInfo;

/**
 *
 * @author kakcura
 */
public class ChartBuilder {

	private SimpleDateFormat dateFormatter = null;
	private JFreeChart chart;

	public ChartBuilder() {
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	}

	public ChartPanel createChartPanel(String title, List<StockInfo> stockInfo, JTextArea updateInfo,
			int first_averaging_period, int second_averaging_period, JButton btnBuy, JLabel lbl_recommendation_date) {

		if(chart != null){
			
		}
		XYDataset priceData = createPriceDataset(stockInfo, first_averaging_period, second_averaging_period);
		IntervalXYDataset volumeData = createVolumeDataset(stockInfo);
		this.chart = createChart(title, priceData, volumeData);

		ChartPanel newChartPanel = new ChartPanel(chart);
		
		newChartPanel.setPreferredSize(new Dimension(100, 200));
		newChartPanel.addChartMouseListener(new ChartMouseListener() {
			public void chartMouseClicked(ChartMouseEvent e) {
				btnBuy.setBackground(Color.WHITE);
				btnBuy.setText("Select a Cross Point");
				lbl_recommendation_date.setText("");
				ChartEntity entity = e.getEntity();
				if (entity != null && entity instanceof XYItemEntity) {
					int index = ((XYItemEntity) entity).getItem();
					StockInfo currentInfo = stockInfo.get(index);
					if (((XYItemEntity) entity).getDataset().getSeriesCount() == 3) {
						if(index > ((XYItemEntity) entity).getDataset().getItemCount(1)){
							index = ((XYItemEntity) entity).getDataset().getItemCount(1) - 1 ; 
						}
						Number moving_avg = ((XYItemEntity) entity).getDataset().getY(1, index);
						Number second_moving_avg = ((XYItemEntity) entity).getDataset().getY(2, index);
						Number price = ((XYItemEntity) entity).getDataset().getY(0, index);
						double close_value = new Double(currentInfo.getClose());
						double price_data = round(price.doubleValue(), 0);
						double first_data = round(moving_avg.doubleValue(), 0);
						double second_data = round(second_moving_avg.doubleValue(), 0);
//						System.out.println("price" + price_data);
//						System.out.println("first" + first_data);
//						System.out.println("second" + second_data);
						if(first_data == second_data){
							Number moving_avg_after = ((XYItemEntity) entity).getDataset().getY(1, index+10);
							Number second_moving_avg_after = ((XYItemEntity) entity).getDataset().getY(2, index+10);
							double first_after = round(moving_avg_after.doubleValue(), 2);
							double second_after = round(second_moving_avg_after.doubleValue(), 2);
							String date = "Recommendation Date : " + currentInfo.getDate() + "\t";
							if(first_after >= second_after){
								btnBuy.setBackground(Color.GREEN);
								lbl_recommendation_date.setText(date);
								btnBuy.setText("Buy");
							}else{
								btnBuy.setBackground(Color.RED);
								lbl_recommendation_date.setText(date);
								btnBuy.setText("Sell");
							}
						}
						
						if(moving_avg.doubleValue() == second_moving_avg.doubleValue()){
							btnBuy.setBackground(Color.GREEN);
						}
//						if (moving_avg.doubleValue() < close_value) {
//							btnBuy.setBackground(Color.GREEN);
//						} else {
//							btnBuy.setBackground(Color.RED);
//						}
					}
				}
			}

			public void chartMouseMoved(ChartMouseEvent e) {
				ChartEntity entity = e.getEntity();
				if (entity != null && entity instanceof XYItemEntity) {
					int index = ((XYItemEntity) entity).getItem();
					StockInfo currentInfo = stockInfo.get(index);
					String date = "Date : " + currentInfo.getDate() + "\t";
					String open = "Open : " + currentInfo.getOpen() + "\t";
					String high = "High : " + currentInfo.getHigh() + "\n";
					String low = "Low : " + currentInfo.getLow() + "\t";
					String close = "Close : " + currentInfo.getClose() + "\t";
					String volume = "Volume : " + currentInfo.getVolume() + "\n";
					String adjClose = "ADJClose : " + currentInfo.getClose();
					String allInfo = date + open + high + low + close + volume + adjClose;
					updateInfo.setText(allInfo);
				}
			}
		});

		return newChartPanel;
	}

	/**
	 * Creates a chart.
	 *
	 * @return a chart.
	 */
	private JFreeChart createChart(String title, XYDataset priceData, IntervalXYDataset volumeData) {

		JFreeChart chart = ChartFactory.createTimeSeriesChart(title, "Date", "Price", priceData, true, true, true);
		XYPlot plot = chart.getXYPlot();
		NumberAxis rangeAxis1 = (NumberAxis) plot.getRangeAxis();
		rangeAxis1.setLowerMargin(0.40); // to leave room for volume bars
		DecimalFormat format = new DecimalFormat("00.00");
		rangeAxis1.setNumberFormatOverride(format);

		NumberAxis rangeAxis2 = new NumberAxis("Volume");
		rangeAxis2.setUpperMargin(1.00); // to leave room for price line

		plot.setRangeAxis(1, rangeAxis2);
		plot.setDataset(1, volumeData);
		plot.setRangeAxis(1, rangeAxis2);
		plot.mapDatasetToRangeAxis(1, 1);
		XYBarRenderer renderer2 = new XYBarRenderer(0.20);
		plot.setRenderer(1, renderer2);
		plot.getRenderer().setSeriesPaint(0, Color.GREEN);
		plot.getRenderer().setSeriesPaint(1, Color.BLUE);
		plot.getRenderer().setSeriesPaint(2, Color.RED);

		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);

		return chart;
	}

	/**
	 * Creates a dataset.
	 *
	 * @return A dataset.
	 */
	private XYDataset createPriceDataset(List<StockInfo> stockInfo, int first_averaging_period, int second_averaging_period) {

		TimeSeries series1 = new TimeSeries("Price");

		try {
			Collections.sort(stockInfo);
			for (StockInfo info : stockInfo) {
				series1.add(new Day(dateFormatter.parse(info.getDate())), Double.parseDouble(info.getClose()));
			}
		} catch (Exception e) {
			System.err.println("Caught Exception: " + e.getMessage());
		}
		TimeSeries first_mav = MovingAverage.createMovingAverage(series1, first_averaging_period + " days moving average",
				first_averaging_period, 0);
		TimeSeries second_mav = MovingAverage.createMovingAverage(series1, second_averaging_period + " days moving average",
				second_averaging_period, 0);
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series1);
		dataset.addSeries(first_mav);
		dataset.addSeries(second_mav);
		return dataset;

	}

	/**
	 * Creates a dataset.
	 *
	 * @return A dataset.
	 */
	private IntervalXYDataset createVolumeDataset(List<StockInfo> stockInfo) {

		TimeSeries series1 = new TimeSeries("Volume");

		try {

			for (StockInfo info : stockInfo) {
				series1.add(new Day(dateFormatter.parse(info.getDate())), Long.parseLong(info.getVolume()));
			}
		} catch (Exception e) {
			System.err.println("Caught Exception: " + e.getMessage());
		}

		return new TimeSeriesCollection(series1);

	}
	
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

}
