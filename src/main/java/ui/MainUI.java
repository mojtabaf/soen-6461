package ui;

import java.awt.CardLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import core.applicationservice.IStockInfoService;
import core.applicationservice.IWatchListService;
import core.applicationservice.StockInfoService;
import core.applicationservice.WatchList;
import core.applicationservice.WatchListService;
import core.model.ErrorCell;
import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.JFileChooser;
import javax.swing.GroupLayout.Alignment;

public class MainUI {

	private JFrame frame;
	private QueryPanel queryPanel = new QueryPanel(false);
	private IStockInfoService stockInfoService = new StockInfoService();
	private JLabel lbl_csv_file = new JLabel("");
	private IWatchListService watchListService = new WatchListService();

	// private ContactUsFrame contactUsFrame = new ContactUsFrame();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUI window = new MainUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 800, 850);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		JMenuItem mntmCSV = new JMenuItem("Browse CSV File");
		mntmCSV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("Browse");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		fileMenu.add(mntmCSV);

		JMenuItem mntmDisplay = new JMenuItem("Display File");
		mntmDisplay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("Display");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		fileMenu.add(mntmDisplay);

		JMenuItem mntmYahoo = new JMenuItem("Get Yahoo Data ");
		mntmYahoo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("Yahoo");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		fileMenu.add(mntmYahoo);

		JMenuItem mntmExit = new JMenuItem("exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("exit");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		fileMenu.add(mntmExit);

		JMenu watchListMenu = new JMenu("Watch List");
		menuBar.add(watchListMenu);

		JMenuItem watchListSetting = new JMenuItem("Watch List Setting");
		watchListSetting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("wathcListSetting");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		watchListMenu.add(watchListSetting);

		JMenuItem saveWathcList = new JMenuItem("Save Setting in File");
		saveWathcList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("saveWathcList");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		watchListMenu.add(saveWathcList);

		JMenuItem importWatchList = new JMenuItem("Import Setting from File");
		importWatchList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("importWatchList");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		watchListMenu.add(importWatchList);

		JMenuItem watchListShow = new JMenuItem("Show Watch List");
		watchListShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					menuPerformed("watchListShow");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		watchListMenu.add(watchListShow);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 47, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		GridBagConstraints gbc_lbl_csv_file = new GridBagConstraints();
		gbc_lbl_csv_file.anchor = GridBagConstraints.WEST;
		gbc_lbl_csv_file.insets = new Insets(0, 0, 5, 0);
		gbc_lbl_csv_file.gridx = 1;
		gbc_lbl_csv_file.gridy = 0;
		frame.getContentPane().add(lbl_csv_file, gbc_lbl_csv_file);

		GridBagConstraints gbc_queryPanel = new GridBagConstraints();
		gbc_queryPanel.fill = GridBagConstraints.BOTH;
		gbc_queryPanel.gridx = 1;
		gbc_queryPanel.gridy = 1;
		frame.getContentPane().add(queryPanel, gbc_queryPanel);

	}

	public void menuPerformed(String menuItem) throws Exception {
		switch (menuItem) {
		case "Browse":
			selectCSV();
			this.cleanChartPanel(true);
			break;
		case "Display":
			this.lbl_csv_file.setVisible(true);
			displayCSV();
			break;
		case "Yahoo":
			this.cleanChartPanel(false);
			this.lbl_csv_file.setText("");
			this.lbl_csv_file.setVisible(false);
			this.queryPanel.setReadFromFile(false);
			this.queryPanel.setFilePath("");
			break;
		case "exit":
			System.exit(0);
			break;
		case "wathcListSetting":
			showWatchListSetting();
			break;
		case "watchListShow":
			showWatchListResult();
			break;
		case "saveWathcList":
			saveWatchListSetting();
			break;
		case "importWatchList":
			importWathcList();
			break;
		}
	}

	private void importWathcList() {
		JFileChooser openFile = new JFileChooser();
		if (openFile.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			String fileName = openFile.getSelectedFile().getAbsolutePath();
			this.watchListService.importWatchListFromFile(fileName);
		}
	}

	private void saveWatchListSetting() {
		JFileChooser openFile = new JFileChooser();
		if (openFile.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			String fileName = openFile.getSelectedFile().getAbsolutePath();
			this.watchListService.saveWatchListToFile(fileName);
		}
	}

	private void showWatchListResult() throws Exception {
		JFrame f = new JFrame("Watch List");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		WatchListUI listUI = new WatchListUI();
		f.getContentPane().add(listUI, BorderLayout.NORTH);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation(dim.width / 2 - 400, dim.height / 2 - 300);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.setSize(500, 600);
		f.setVisible(true);
	}

	private void showWatchListSetting() {
		JFrame f = new JFrame("Add and Remove");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		WatchListSetting dual = new WatchListSetting();
		WatchList watchList = WatchList.getInstance();
		String[] source = watchList.getSources().toArray(new String[0]);
		dual.addSourceElements(source);
		String[] destination = watchList.getWatchSources().toArray(new String[0]);
		dual.addDestinationElements(destination);
		f.getContentPane().add(dual, BorderLayout.CENTER);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation(dim.width / 2 - 400, dim.height / 2 - 300);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.setSize(470, 370);
		f.setVisible(true);
	}

	private void displayCSV() {
		try {
			if (lbl_csv_file.getText() == "") {
				JOptionPane.showMessageDialog(null, "Please Select your CSV file!", "Warning",
						JOptionPane.WARNING_MESSAGE);
			} else {
				String path = lbl_csv_file.getText().split(" ")[2].trim();
				Desktop.getDesktop().open(new File(path));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void selectCSV() {
		JFileChooser openFile = new JFileChooser();
		if (openFile.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			String fileName = openFile.getSelectedFile().getAbsolutePath();
			if (!stockInfoService.isValid(fileName)) {
				JOptionPane.showMessageDialog(null, "selected file is not valid csv file or it might be corrupted!",
						"Warning", JOptionPane.WARNING_MESSAGE);
			} else {
				List<ErrorCell> errors = stockInfoService.getCorruptedCells(fileName);
				if (errors.size() > 0) {
					String str = "";
					for (ErrorCell errorCell : errors) {
						if (errorCell.getCol() != 0) {
							str += "Cell --> [" + errorCell.getRow() + " , " + errorCell.getCol() + "]\n";
						} else {
							str += "Cell --> Row number " + errorCell.getRow() + " has more or less columns\n";
						}

					}
					JOptionPane.showMessageDialog(null, "Your file has some missing Cells!\n" + str, "Warning",
							JOptionPane.WARNING_MESSAGE);
				} else {
					this.lbl_csv_file.setVisible(true);
					lbl_csv_file.setText("Selected File: " + fileName);
				}
			}
		}
	}

	private void cleanChartPanel(boolean fromFile) {
		if (this.queryPanel != null) {
			if (this.queryPanel.getChartPanel() != null)
				this.queryPanel.remove(this.queryPanel.getChartPanel());
			if (this.queryPanel.getBtnBuy() != null)
				this.queryPanel.remove(this.queryPanel.getBtnBuy());
			if (this.queryPanel.getTextArea() != null)
				this.queryPanel.remove(this.queryPanel.getTextArea());
			this.frame.remove(queryPanel);
			this.queryPanel.revalidate();
			this.queryPanel.repaint();
			this.frame.revalidate();
			this.frame.repaint();
		}
		this.queryPanel = new QueryPanel(fromFile);
		if (!this.lbl_csv_file.getText().equalsIgnoreCase("")) {
			String tempString = this.lbl_csv_file.getText().trim();
			this.queryPanel.setFilePath(tempString.substring(tempString.indexOf(" ") + 1).split(" ", 2)[1]);
		}
		this.queryPanel.setReadFromFile(true);
		GridBagConstraints gbc_queryPanel = new GridBagConstraints();
		gbc_queryPanel.fill = GridBagConstraints.BOTH;
		gbc_queryPanel.gridx = 1;
		gbc_queryPanel.gridy = 1;
		this.queryPanel.revalidate();
		this.queryPanel.repaint();
		frame.getContentPane().add(queryPanel, gbc_queryPanel);
		this.frame.revalidate();
		this.frame.repaint();

	}
}