package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import core.applicationservice.IWatchListService;
import core.applicationservice.WatchList;
import core.applicationservice.WatchListService;

public class WatchListUI extends JPanel{
	private static final long serialVersionUID = 1L;
	private Map<String, Boolean> watchListResult = new HashMap<>();
	private IWatchListService listService = new WatchListService();
	private BufferedImage upImage = ImageIO.read(new File("./images/up_arrow.png"));
	private BufferedImage downImage = ImageIO.read(new File("./images/down_arrow.png"));
	
	public WatchListUI() throws Exception {
	
		WatchList watchList = WatchList.getInstance();
		List<String> companies_name = watchList.getWatchSources();
		if(companies_name.size() > 0){
			this.watchListResult = this.listService.getWatchListDetails(companies_name,
					365, 20, 100);
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[]{0, 0, 0};
			gridBagLayout.rowHeights = new int[]{0, 0, 0};
			gridBagLayout.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			setLayout(gridBagLayout);
			Date current_date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String current_date_text = dateFormat.format(current_date);
			JLabel lblCurrent = new JLabel("Today: " + current_date_text);
			Font font = new Font("", Font.BOLD,14);
			lblCurrent.setFont(font);
			
			GridBagConstraints gbc_lblCurrent = new GridBagConstraints();
			gbc_lblCurrent.insets = new Insets(0, 0, 0, 0);
			gbc_lblCurrent.gridx = 1;
			gbc_lblCurrent.gridy = 1;
			gbc_lblCurrent.gridwidth = 4;
			gbc_lblCurrent.anchor = GridBagConstraints.CENTER;
			add(lblCurrent, gbc_lblCurrent);
			
			int index = 2;
			for(Entry<String, Boolean> company: watchListResult.entrySet()){
				JLabel lblCompany_name = new JLabel(company.getKey());
				GridBagConstraints gbc_lblCompany_name = new GridBagConstraints();
				gbc_lblCompany_name.insets = new Insets(5, 5, 0, 0);
				gbc_lblCompany_name.gridx = 1;
				gbc_lblCompany_name.gridy = index;
				gbc_lblCompany_name.anchor = GridBagConstraints.LINE_START;
				add(lblCompany_name, gbc_lblCompany_name);
				
				JLabel lblShouldBuy = new JLabel(company.getValue()? new ImageIcon(upImage): new ImageIcon(downImage));
				GridBagConstraints gbc_lblShouldBuy = new GridBagConstraints();
				gbc_lblShouldBuy.insets = new Insets(5, 0, 0, 5);
				gbc_lblShouldBuy.gridx = 4;
				gbc_lblShouldBuy.gridy = index;
				add(lblShouldBuy, gbc_lblShouldBuy);
				index++;
			}
		}
		
	}
}
